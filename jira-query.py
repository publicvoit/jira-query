#!/usr/bin/env python3
# -*- coding: utf-8 -*-
PROG_VERSION = "Time-stamp: <2021-11-18 18:15:10 vk>"

import sys
import os
import argparse
import re
import logging

CONFIGDIR = os.getcwd()
CONFIGFILEBASENAME = 'jiraconfig'
CONFIGFILENAME = os.path.join(CONFIGDIR, CONFIGFILEBASENAME)

CONFIGTEMPLATE = '''
JIRA_USER = 'myusername'
JIRA_PASSWORD = 'mysupersecurepasswordforjira'
JIRA_URL = 'https://jira.example.com/'
'''


try:
    sys.path.insert(0, CONFIGDIR)  # add cwd to Python path in order to find config file
    import jiraconfig  # here, I was not able to use the CONFIGFILENAME variable
except ImportError:
    print("\nERROR: Could not find \"" + CONFIGFILENAME + \
        "\".\nPlease generate such a file in the " + \
        "same directory as this script with following content and configure accordingly:\n" + \
        CONFIGTEMPLATE)
    sys.exit(11)


DESCRIPTION = '''This tool executes a Jira query and returns various data.

A file "jiraconfig.py" (in the same dir as the Python source) is 
required with the content similar to the template below. Please be 
extra careful with the permissions on this file because it contains
your Jira credentials in cleartext:

''' + CONFIGTEMPLATE

EPILOG='''autor:      Karl Voit <tools@Karl-Voit.at>
license:    GPL v3 or any later version
URL:        https://gitlab.com/publicvoit/jira-query
bugreports: https://gitlab.com/publicvoit/jira-query/-/issues
version:    ''' + PROG_VERSION + '\n'

parser = argparse.ArgumentParser(prog=sys.argv[0],
                                 # keep line breaks in EPILOG and such
                                 formatter_class=argparse.RawDescriptionHelpFormatter,
                                 epilog=EPILOG,
                                 description=DESCRIPTION)

verbosity_group = parser.add_mutually_exclusive_group()

verbosity_group.add_argument('--version', action='version',
                             version=PROG_VERSION, help="Display version and exit")

verbosity_group.add_argument("--verbose",
                    dest="verbose", action="store_true",
                    help="Enable verbose mode")

verbosity_group.add_argument("--quiet",
                    dest="quiet", action="store_true",
                    help="Enable quiet mode")


parser.add_argument('jquery', metavar='JIRAQUERY', nargs=1,
                    help='Jira query string')


action_group = parser.add_mutually_exclusive_group(required=True)

action_group.add_argument("--sum",
                    dest="sum", action="store_true",
                    help="Return the sum of the matching tickets")

action_group.add_argument("--keys",
                    dest="keys", action="store_true",
                    help="Return the list of Jira ticket keys of the matching tickets")


args = parser.parse_args()


def handle_logging():
    """Log handling and configuration"""

    if args.verbose:
        FORMAT = "%(levelname)-8s %(asctime)-15s %(message)s"
        logging.basicConfig(level=logging.DEBUG, format=FORMAT)
    elif args.quiet:
        FORMAT = "%(levelname)-8s %(message)s"
        logging.basicConfig(level=logging.ERROR, format=FORMAT)
    else:
        FORMAT = "%(levelname)-8s %(message)s"
        logging.basicConfig(level=logging.INFO, format=FORMAT)


def retrieve_tickets_from_jira(query):

    try:
        from jira import JIRA
    except ImportError:
        print("ERROR: Could not find Python module \"JIRA\".\nPlease install it, e.g., with \"sudo pip install jira\".")
        sys.exit(12)

    jira = JIRA(jiraconfig.JIRA_URL, basic_auth=(jiraconfig.JIRA_USER, jiraconfig.JIRA_PASSWORD))

    logging.debug('query: ' + query)
    tickets = jira.search_issues(query)

    logging.debug('found ' + str(len(tickets)) + ' issues')

    return tickets


def main():
    """Main function"""

    handle_logging()

    tickets = retrieve_tickets_from_jira(args.jquery[0])
    
    if args.sum:
        number = str(len(tickets))
        print(str(number))
    elif args.keys:
        print((' ').join(list(ticket.key for ticket in tickets)))
    
if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:

        logging.info("Received KeyboardInterrupt")

# END OF FILE #################################################################
